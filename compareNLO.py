variables = [
("HT", "H_{T} [GeV]"),
("MET", "Missing E_{T} [GeV]"),
("N_bjet","b-jet multiplicity"),
("N_jet", "Jet multiplicity"),
("dPhi_lepph", "\\Delta \\phi(\\ell, \\gamma)"),
("dPhi_topph", "\\Delta \\phi(t, \\gamma)"),
("dR_lepph","\\Delta R(\\ell, \\gamma)"),
("dR_topph", "\\Delta R(t, \\gamma)"),
("jet_eta", "Leading jet #eta"),
("jet_pT", "Leading jet p_{T} [GeV]"),
("lepton_eta", "Lepton #eta"),
("lepton_pT", "Lepton p_{T} [GeV]"),
("m_topph", "Mass_{t#gamma} [GeV]"),
("pT_topph", "p_{T}^{t#gamma} [GeV]"),
("photon_eta", "#eta_{#gamma}"),
("photon_pT", "p^{#gamma}_{T} [GeV]"),
("top_m", "top quark mass [GeV]"),
("top_pT", "top quark p_{T} [GeV]"),
("top_rap", "top quark rapidity (y)"),
]

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.SetAtlasStyle()


formats = [".png", ".pdf", ".eps", ".root"]

path = "/code/harish/tgamma/rivet-tqgamma/"
fnlo = ROOT.TFile().Open(path+"/nominal.root")
flo = ROOT.TFile().Open(path+"/tqGammaLO/nominal.root")


for var in variables:
    can = ROOT.TCanvas("can","can",0,0, 700,500)

    pad0 = ROOT.TPad("pad0","pad0",0,0.21,1,1,0,0,0);
    pad0.SetTicks(1,1);
    pad0.SetTopMargin(0.05);
    pad0.SetBottomMargin(0.1);
    pad0.SetLeftMargin(0.14);
    pad0.SetRightMargin(0.05);
    pad0.SetFrameBorderMode(0);
    pad0.SetFillStyle(0);

    pad1 = ROOT.TPad("pad1","pad1",0,0,1,0.28,0,0,0);
    pad1.SetTicks(1,1);
    pad1.SetTopMargin(0.);
    pad1.SetBottomMargin(0.37);
    pad1.SetLeftMargin(0.14);
    pad1.SetRightMargin(0.05);
    pad1.SetFrameBorderMode(0);
    pad1.SetFillStyle(0);
    pad1.Draw()
    pad0.Draw()
    pad0.cd()

    hname = var[0]
    nlo = fnlo.Get("Ac_tqGamma/"+hname)
    lo = flo.Get("Ac_tqGamma/"+hname)


    hratio = nlo.Clone("ratio")
    hratio.Divide(lo)
    hratio.SetMarkerStyle(21);
    hratio.SetMarkerSize(0.0);
    hratio.SetLineColor(ROOT.kBlue);
    hratio.SetLineWidth(2);
    hratio.SetFillStyle(0);
    hratio.SetMinimum(0.0)
    hratio.SetMaximum(2.2)
    
    hratio.GetYaxis().SetTitle("Ratio")
    hratio.GetYaxis().CenterTitle()
    hratio.GetYaxis().SetNdivisions(505)
    hratio.GetYaxis().SetTitleSize(16)
    hratio.GetYaxis().SetTitleFont(43)
    hratio.GetYaxis().SetTitleOffset(1.55);
    hratio.GetYaxis().SetLabelFont(43);
    hratio.GetYaxis().SetLabelSize(15);

    hratio.GetXaxis().SetTitle(var[1])
    hratio.GetXaxis().SetNdivisions(nlo.GetXaxis().GetNdivisions())
    hratio.GetXaxis().SetTitleSize(20);
    hratio.GetXaxis().SetTitleFont(43);
    hratio.GetXaxis().SetTitleOffset(4.);
    hratio.GetXaxis().SetLabelFont(43);
    hratio.GetXaxis().SetLabelSize(15);
    


    leg = ROOT.TLegend(0.63,0.60,0.78,0.75)
    names = ["tq#gamma NLO", "tq#gamma LO production only"]
    nlo.SetMarkerSize(0.0)
    nlo.SetMarkerStyle(21)
    leg.AddEntry(nlo, "tq#gamma NLO")
    nlo.SetMaximum(1.7*nlo.GetMaximum())
    nlo.GetYaxis().SetTitle("Cross section / bin")
    nlo.GetXaxis().SetLabelSize(0.0)
    nlo.SetLineColor(ROOT.kBlue)
    nlo.Draw("hist e")

    lo.SetMarkerSize(0.0)
    lo.SetMarkerStyle(21)
    leg.AddEntry(lo, "tq#gamma LO")
    lo.SetLineColor(ROOT.kRed)
    lo.Draw("hist e SAME")

    ROOT.ATLASLabel(0.2, 0.82, '  Simulation Internal', ROOT.kBlack)
    ROOT.myText(0.2,0.73,ROOT.kBlack, "#sqrt{s} = 13 TeV")
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(30*leg.GetTextSize())
    leg.Draw()
    pad1.cd()
    hratio.Draw("e")
        
    myline = ROOT.TLine(nlo.GetXaxis().GetXmin(), 1, nlo.GetXaxis().GetXmax(), 1)
    myline.SetLineWidth(2)
    myline.SetLineColor(ROOT.kBlack)
    myline.SetLineStyle(ROOT.kDashed)
    myline.Draw("SAME")
    can.RedrawAxis()

    for format in formats:
        can.SaveAs("NLOPlotsv3/"+var[0]+format)
