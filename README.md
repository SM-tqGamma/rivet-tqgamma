source setup.sh

rivet-buildplugin RivetAc_tqGamma.so Ac_tqGamma.cc

check for errors-------------

output: RivetAc_tqGamma.so

check the following in RunRivet.py:
theApp.EvtMax
InputFolder(remember to the '/' at the end for the path)
OutputRoot
OutputYoda
rivet.Analyses
rivet.CrossSection (if doing an absolute analysis, not normalised analysis)

athena RunRivet.py

Outputs:
Output.root
Output.yoda

If using yoda file:
cd MakePlots;
rivet-mkhtml ../Output_tqGamma_nom.yoda:'Title=Pythia8':'LineWidth=0.06':'LineStyle=solid' ../Output_tqGamma_H7.yoda:'Title=Herwig7':'LineWidth=0.06':'LineStyle=dashed' ../Output_tqGamma_Hpp.yoda:'Title=Herwig++':'LineWidth=0.06':'LineStyle=dashed'

Otherwise use the root file
