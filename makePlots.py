variables = [
("HT", "H_{T} [GeV]"),
("MET", "Missing E_{T} [GeV]"),
("N_bjet","b-jet multiplicity"),
("N_jet", "Jet multiplicity"),
("dPhi_lepph", "\\Delta \\phi(\\ell, \\gamma)"),
("dPhi_topph", "\\Delta \\phi(t, \\gamma)"),
("dR_lepph","\\Delta R(\\ell, \\gamma)"),
("dR_topph", "\\Delta R(t, \\gamma)"),
("jet_eta", "Leading jet #eta"),
("jet_pT", "Leading jet p_{T} [GeV]"),
("lepton_eta", "Lepton #eta"),
("lepton_pT", "Lepton p_{T} [GeV]"),
("m_topph", "Mass_{t#gamma} [GeV]"),
("pT_topph", "p_{T}^{t#gamma} [GeV]"),
("photon_eta", "#eta_{#gamma}"),
("photon_pT", "p^{#gamma}_{T} [GeV]"),
("top_m", "top quark mass [GeV]"),
("top_pT", "top quark p_{T} [GeV]"),
("top_rap", "top quark rapidity (y)"),
]

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.SetAtlasStyle()


formats = [".png", ".pdf", ".eps", ".root"]

path = "/code/harish/tgamma/rivet-tqgamma/"
fnom = ROOT.TFile().Open(path+"/nominal.root")
fmur0p5 = ROOT.TFile().Open(path+"/mur0p5.root")
fmur2 = ROOT.TFile().Open(path+"/mur2.root")
fvar3cup = ROOT.TFile().Open(path+"/var3cup.root")
fvar3cdown = ROOT.TFile().Open(path+"/var3cdown.root")
for var in variables:
    hname = var[0]
    nominal = fnom.Get("Ac_tqGamma/"+hname)
    prod = fnom.Get("Ac_tqGamma/"+hname+"_prod")
    decay = fnom.Get("Ac_tqGamma/"+hname+"_decay")
    mur2 = fmur2.Get("Ac_tqGamma/"+hname)
    mur0p5 = fmur0p5.Get("Ac_tqGamma/"+hname)
    var3cup = fvar3cup.Get("Ac_tqGamma/"+hname)
    var3cdown = fvar3cdown.Get("Ac_tqGamma/"+hname)
    
    hlist = [nominal, prod, decay,mur2, mur0p5, var3cup, var3cdown]
    col1 = ROOT.TColor(9001,248,151,31)
    col2 = ROOT.TColor(9002,87,157,66)
    col3 = ROOT.TColor(9003,0,169,183)
    col4 = ROOT.TColor(9004,238,31,96)

    clist =[0, ROOT.kRed, ROOT.kCyan, ROOT.kGreen, ROOT.kOrange, ROOT.kBlue, ROOT.kMagenta]
    can = ROOT.TCanvas("can","can",0,0, 700,500)
    
    leg = ROOT.TLegend(0.63,0.60,0.93,0.90)
    names = ["tq#gamma Nominal", "tq#gamma production only", "tq#gamma decay only", "#mu_{R}=#mu_{F}=2.0", "#mu_{R}=#mu_{F}=0.5", "var3cUp", "var3cDown"]
    for hist, col, name in zip(hlist,clist, names):
        hist.SetMarkerSize(0.0)
        hist.SetMarkerStyle(21)
        leg.AddEntry(hist, name)
        if hist== nominal:
            hist.SetMaximum(1.7*hist.GetMaximum())
            hist.GetYaxis().SetTitle("Cross section / bin")
            hist.GetXaxis().SetTitle(var[1])
            hist.Draw("hist e")
        else:
            hist.SetLineColor(col)
            hist.Draw("hist e SAME")

    ROOT.ATLASLabel(0.2, 0.82, '      Simulation Internal', ROOT.kBlack)
    ROOT.myText(0.2,0.73,ROOT.kBlack, "#sqrt{s} = 13 TeV")
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(30*leg.GetTextSize())
    leg.Draw()
    for format in formats:
        can.SaveAs("Plots/"+var[0]+format)
