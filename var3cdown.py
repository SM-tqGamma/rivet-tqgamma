theApp.EvtMax = 1000000
import os,sys
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool 
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()


InputFolder = "/data_ceph/harish/tqGamma/mc15_13TeV.412089.MadGraphPythia8EvtGen_A14NNPDF23_tqgammaSM_tchan_4fl.evgen.EVNT.e7266/"
OutputRoot  = "var3cdown.root" 
OutputYoda  = "var3cdown.yoda"
FileList = os.listdir(InputFolder) 
for file in FileList: 
    if not ".pool.root" in file and not "EVNT.root" in file: 
        continue 
    inputFile = InputFolder+file 
    if os.path.exists(inputFile): 
        svcMgr.EventSelector.InputCollections.append(inputFile) 

rivet = Rivet_i("var3cdown")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'Ac_tqGamma' ]
rivet.HistoFile = OutputYoda
rivet.WeightName= "Var3cDown"
#rivet.Stream = "Nominal"
#rivet.RunName = ""
rivet.DoRootHistos = True
rivet.CrossSection = 1.389
rivet.IgnoreBeamCheck = True
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"] 
