variables = [
("HT", "H_{T} [GeV]"),
("MET", "Missing E_{T} [GeV]"),
("N_bjet","b-jet multiplicity"),
("N_jet", "Jet multiplicity"),
("dPhi_lepph", "\\Delta \\phi(\\ell, \\gamma)"),
("dPhi_topph", "\\Delta \\phi(t, \\gamma)"),
("dR_lepph","\\Delta R(\\ell, \\gamma)"),
("dR_topph", "\\Delta R(t, \\gamma)"),
("jet_eta", "Leading jet #eta"),
("jet_pT", "Leading jet p_{T} [GeV]"),
("lepton_eta", "Lepton #eta"),
("lepton_pT", "Lepton p_{T} [GeV]"),
("m_topph", "Mass_{t#gamma} [GeV]"),
("pT_topph", "p_{T}^{t#gamma} [GeV]"),
("photon_eta", "#eta_{#gamma}"),
("photon_pT", "p^{#gamma}_{T} [GeV]"),
("top_m", "top quark mass [GeV]"),
("top_pT", "top quark p_{T} [GeV]"),
("top_rap", "top quark rapidity (y)"),
]

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.SetAtlasStyle()


formats = [".png", ".pdf", ".eps", ".root"]

path = "/code/harish/tgamma/rivet-tqgamma/"
fnom = ROOT.TFile().Open(path+"/nominal.root")
fmur0p5 = ROOT.TFile().Open(path+"/mur0p5.root")
fmur2 = ROOT.TFile().Open(path+"/mur2.root")
fherwig = ROOT.TFile().Open(path+"/herwig.root")

for var in variables:
    can = ROOT.TCanvas("can","can",0,0, 700,500)

    pad0 = ROOT.TPad("pad0","pad0",0,0.20,1,1,0,0,0);
    pad0.SetTicks(1,1);
    pad0.SetTopMargin(0.05);
    pad0.SetBottomMargin(0.1);
    pad0.SetLeftMargin(0.14);
    pad0.SetRightMargin(0.05);
    pad0.SetFrameBorderMode(0);
    pad0.SetFillStyle(0);

    pad1 = ROOT.TPad("pad1","pad1",0,0,1,0.28,0,0,0);
    pad1.SetTicks(1,1);
    pad1.SetTopMargin(0.0);
    pad1.SetBottomMargin(0.37);
    pad1.SetLeftMargin(0.14);
    pad1.SetRightMargin(0.05);
    pad1.SetFrameBorderMode(0);
    pad1.SetFillStyle(0);
    pad1.Draw()
    pad0.Draw()
    pad0.cd()

    hname = var[0]
    nominal = fnom.Get("Ac_tqGamma/"+hname)
    #prod = fnom.Get("Ac_tqGamma/"+hname+"_prod")
    #decay = fnom.Get("Ac_tqGamma/"+hname+"_decay")
    mur2 = fmur2.Get("Ac_tqGamma/"+hname)
    mur0p5 = fmur0p5.Get("Ac_tqGamma/"+hname)
    #herwig = fherwig.Get("Ac_tqGamma/"+hname)
    #var3cup = fvar3cup.Get("Ac_tqGamma/"+hname)
    #var3cdown = fvar3cdown.Get("Ac_tqGamma/"+hname)

    hlist = [nominal, mur2, mur0p5]#, herwig]
    col1 = ROOT.TColor(9001,248,151,31)
    col2 = ROOT.TColor(9002,87,157,66)
    col3 = ROOT.TColor(9003,0,169,183)
    col4 = ROOT.TColor(9004,238,31,96)

    clist =[0, ROOT.kRed, ROOT.kBlue]#, ROOT.kBlue]

    hrlist = []
    for hist,col in zip(hlist,clist):
        hratio = hist.Clone(hist.GetName()+"ratio")
        hratio.Divide(nominal)
        hratio.SetMarkerStyle(21);
        hratio.SetMarkerSize(0.0);
        hratio.SetLineColor(col);
        hratio.SetLineWidth(2);
        hratio.SetFillStyle(0);
        hratio.SetMinimum(0.5)
        hratio.SetMaximum(1.5)
        
        hratio.GetYaxis().SetTitle("Ratio")
        hratio.GetYaxis().CenterTitle()
        hratio.GetYaxis().SetNdivisions(505)
        hratio.GetYaxis().SetTitleSize(16)
        hratio.GetYaxis().SetTitleFont(43)
        hratio.GetYaxis().SetTitleOffset(1.55);
        hratio.GetYaxis().SetLabelFont(43);
        hratio.GetYaxis().SetLabelSize(15);

        hratio.GetXaxis().SetTitle(var[1])
        hratio.GetXaxis().SetNdivisions(hist.GetXaxis().GetNdivisions())
        hratio.GetXaxis().SetTitleSize(20);
        hratio.GetXaxis().SetTitleFont(43);
        hratio.GetXaxis().SetTitleOffset(4.);
        hratio.GetXaxis().SetLabelFont(43);
        hratio.GetXaxis().SetLabelSize(15);
        hrlist.append(hratio)


    leg = ROOT.TLegend(0.63,0.60,0.83,0.80)
    names = ["tq#gamma NLO Nominal", "#mu_{R}=#mu_{F}=2.0", "#mu_{R}=#mu_{F}=0.5"]#, "MadGraph5+Herwig7"]
    for hist, col, name in zip(hlist,clist, names):
        hist.SetMarkerSize(0.0)
        hist.SetMarkerStyle(21)
        leg.AddEntry(hist, name)
        if hist== nominal:
            hist.SetMaximum(1.7*hist.GetMaximum())
            hist.GetYaxis().SetTitle("Cross section / bin")
            #hist.GetXaxis().SetTitle(var[1])
            hist.GetXaxis().SetLabelSize(0.0)
            hist.Draw("hist e")
        else:
            hist.SetLineColor(col)
            hist.Draw("hist e SAME")

    ROOT.ATLASLabel(0.2, 0.82, '      Simulation Internal', ROOT.kBlack)
    ROOT.myText(0.2,0.73,ROOT.kBlack, "#sqrt{s} = 13 TeV")
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(30*leg.GetTextSize())
    leg.Draw()
    pad1.cd()
    hrlist[1].Draw("e")
    hrlist[2].Draw("e SAME")
        
    myline = ROOT.TLine(nominal.GetXaxis().GetXmin(), 1, nominal.GetXaxis().GetXmax(), 1)
    myline.SetLineWidth(2)
    myline.SetLineColor(ROOT.kBlack)
    myline.SetLineStyle(ROOT.kDashed)
    myline.Draw("SAME")
    #can.RedrawAxis()

    for format in formats:
        can.SaveAs("NLOPlotsv2/"+var[0]+format)
