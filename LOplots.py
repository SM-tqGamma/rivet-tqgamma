variables = [
("HT", "H_{T} [GeV]"),
("MET", "Missing E_{T} [GeV]"),
("N_bjet","b-jet multiplicity"),
("N_jet", "Jet multiplicity"),
("dPhi_lepph", "\\Delta \\phi(\\ell, \\gamma)"),
("dPhi_topph", "\\Delta \\phi(t, \\gamma)"),
("dR_lepph","\\Delta R(\\ell, \\gamma)"),
("dR_topph", "\\Delta R(t, \\gamma)"),
("jet_eta", "Leading jet #eta"),
("jet_pT", "Leading jet p_{T} [GeV]"),
("lepton_eta", "Lepton #eta"),
("lepton_pT", "Lepton p_{T} [GeV]"),
("m_topph", "Mass_{t#gamma} [GeV]"),
("pT_topph", "p_{T}^{t#gamma} [GeV]"),
("photon_eta", "#eta_{#gamma}"),
("photon_pT", "p^{#gamma}_{T} [GeV]"),
("top_m", "top quark mass [GeV]"),
("top_pT", "top quark p_{T} [GeV]"),
("top_rap", "top quark rapidity (y)"),
]

import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(1)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.SetAtlasStyle()

def setRatioStyle(hratio, col, xtitle, htitle="Ratio", ymin=0.0, ymax=2.0,lblsize=15):
    hratio.SetMarkerStyle(21);
    hratio.SetMarkerSize(0.0);
    hratio.SetLineColor(col);
    hratio.SetLineWidth(2);
    hratio.SetFillStyle(0);
    hratio.SetMinimum(ymin)
    hratio.SetMaximum(ymax)
    
    hratio.GetYaxis().SetTitle(htitle)
    hratio.GetYaxis().CenterTitle()
    hratio.GetYaxis().SetNdivisions(505)
    hratio.GetYaxis().SetTitleSize(16)
    hratio.GetYaxis().SetTitleFont(43)
    #hratio.GetYaxis().SetTitleOffset(1.55);
    hratio.GetYaxis().SetLabelFont(43);
    hratio.GetYaxis().SetLabelSize(15);

    hratio.GetXaxis().SetTitle(xtitle)
    hratio.GetXaxis().SetNdivisions(hist.GetXaxis().GetNdivisions())
    hratio.GetXaxis().SetTitleSize(20);
    hratio.GetXaxis().SetTitleFont(43);
    hratio.GetXaxis().SetTitleOffset(4.);
    hratio.GetXaxis().SetLabelFont(43);
    hratio.GetXaxis().SetLabelSize(0);
    return hratio



formats = [".png", ".pdf", ".eps", ".root"]

path = "/code/harish/tgamma/rivet-tqgamma/tqGammaLO/"
fnom = ROOT.TFile().Open(path+"/nominal.root")
fmur0p5 = ROOT.TFile().Open(path+"/mur0p5.root")
fmur2 = ROOT.TFile().Open(path+"/mur2.root")
fvar3cup = ROOT.TFile().Open(path+"/var3cup.root")
fvar3cdown = ROOT.TFile().Open(path+"/var3cdown.root")

for var in variables:
    can = ROOT.TCanvas("can","can",0,0, 900,900)

    pad0 = ROOT.TPad("pad0","pad0",0,0.29,1,1,0,0,0);
    pad0.SetTicks(1,1);
    pad0.SetTopMargin(0.05);
    pad0.SetBottomMargin(0.1);
    pad0.SetLeftMargin(0.14);
    pad0.SetRightMargin(0.05);
    pad0.SetFrameBorderMode(0);
    pad0.SetFillStyle(0);

    pad1 = ROOT.TPad("pad1","pad1",0,0.20,1,0.35,0,0,0);
    pad1.SetTicks(1,1);
    pad1.SetTopMargin(0.0);
    pad1.SetBottomMargin(0.1);
    pad1.SetLeftMargin(0.14);
    pad1.SetRightMargin(0.05);
    pad1.SetFrameBorderMode(0);
    pad1.SetFillStyle(0);

    pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.21,0,0,0);
    pad2.SetTicks(1,1);
    pad2.SetTopMargin(0.0);
    pad2.SetBottomMargin(0.37);
    pad2.SetLeftMargin(0.14);
    pad2.SetRightMargin(0.05);
    pad2.SetFrameBorderMode(0);
    pad2.SetFillStyle(0);

    pad2.Draw()
    pad1.Draw()
    pad0.Draw()
    pad0.cd()

    hname = var[0]
    nominal = fnom.Get("Ac_tqGamma/"+hname)
    prod = fnom.Get("Ac_tqGamma/"+hname+"_prod")
    decay = fnom.Get("Ac_tqGamma/"+hname+"_decay")
    mur2 = fmur2.Get("Ac_tqGamma/"+hname)
    mur0p5 = fmur0p5.Get("Ac_tqGamma/"+hname)
    var3cup = fvar3cup.Get("Ac_tqGamma/"+hname)
    var3cdown = fvar3cdown.Get("Ac_tqGamma/"+hname)

    hlist = [nominal, prod, decay, mur2, mur0p5, var3cup, var3cdown]
    col1 = ROOT.TColor(9001,248,151,31)
    col2 = ROOT.TColor(9002,87,157,66)
    col3 = ROOT.TColor(9003,0,169,183)
    col4 = ROOT.TColor(9004,238,31,96)

    clist =[0, ROOT.kRed, ROOT.kCyan, ROOT.kGreen, ROOT.kOrange, ROOT.kBlue, ROOT.kMagenta]

    hrlist = []
    for hist,col in zip(hlist[3:],clist[3:]):
        hratio = hist.Clone(hist.GetName()+"ratio")
        hratio.Divide(nominal)
        hr = setRatioStyle(hratio, col, "", htitle="Ratio", ymin=0.7, ymax=1.3, lblsize=0.0)
        hrlist.append(hr)

    prodratio = prod.Clone("prodratio")
    prodratio.Divide(decay)
    pdr = setRatioStyle(prodratio, clist[1], var[1], htitle="#frac{Production}{Decay}",ymin=0.0, ymax=21)


    leg = ROOT.TLegend(0.63,0.60,0.93,0.90)
    names = ["tq#gamma LO Nominal", "tq#gamma LO production only", "tq#gamma LO decay only", "#mu_{R}=#mu_{F}=2.0", "#mu_{R}=#mu_{F}=0.5", "var3cUp", "var3cDown"]

    for hist, col, name in zip(hlist,clist, names):
        hist.SetMarkerSize(0.0)
        hist.SetMarkerStyle(21)
        leg.AddEntry(hist, name)
        if hist== nominal:
            hist.SetMaximum(1.7*hist.GetMaximum())
            hist.GetYaxis().SetTitle("Cross section / bin")
            #hist.GetXaxis().SetTitle(var[1])
            hist.GetXaxis().SetLabelSize(0.0)
            hist.Draw("hist e")
        else:
            hist.SetLineColor(col)
            hist.Draw("hist e SAME")

    ROOT.ATLASLabel(0.2, 0.82, 'Simulation Internal', ROOT.kBlack)
    ROOT.myText(0.2,0.73,ROOT.kBlack, "#sqrt{s} = 13 TeV")
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(30*leg.GetTextSize())
    leg.Draw()
    pad1.cd()
    for hr in hrlist:
        hr.Draw("e SAME")
        
    myline = ROOT.TLine(nominal.GetXaxis().GetXmin(), 1, nominal.GetXaxis().GetXmax(), 1)
    myline.SetLineWidth(2)
    myline.SetLineColor(ROOT.kBlack)
    myline.SetLineStyle(ROOT.kDashed)
    myline.Draw("SAME")
    pad2.cd()
    pdr.Draw("e")
    myline.Draw("SAME")
    can.RedrawAxis()

    for format in formats:
        can.SaveAs("LOPlotsv2/"+var[0]+format)
