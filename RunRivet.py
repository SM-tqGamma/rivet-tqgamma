theApp.EvtMax = 1000000
import os,sys
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
import AthenaPoolCnvSvc.ReadAthenaPool 
from AthenaCommon.AlgSequence import AlgSequence
from Rivet_i.Rivet_iConf import Rivet_i
from GaudiSvc.GaudiSvcConf import THistSvc

svcMgr.EventSelector.InputCollections =[]
svcMgr.EventSelector.SkipBadFiles = True
svcMgr.EventSelector.BackNavigation = True
job = AlgSequence()


InputFolder = "/data_ceph/harish/tqGamma/mc15_13TeV.412089.MadGraphPythia8EvtGen_A14NNPDF23_tqgammaSM_tchan_4fl.evgen.EVNT.e7266/"
OutputRoot  = "nominal.root" 
OutputYoda  = "nominal.yoda"
FileList = os.listdir(InputFolder) 
for file in FileList: 
    if not ".pool.root" in file and not "EVNT.root" in file: 
        continue 
    inputFile = InputFolder+file 
    if os.path.exists(inputFile): 
        svcMgr.EventSelector.InputCollections.append(inputFile) 

'''
from PyUtils import AthFile
af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])

metadata = af.fileinfos['metadata']
if '/Generation/Parameters' in metadata:
    genpars=metadata['/Generation/Parameters']
    if 'HepMCWeightNames' in genpars:
        systWeights=genpars['HepMCWeightNames']
    else:
        print 'HepMCWeightName not found in /Generation/Parameters:'
        print genpars
        raise RuntimeError('HepMCWeightName not found in /Generation/Parameters. Exiting...')
else:
    print '/Generation/Parameters not found in metadata:'
    print metadata
    raise RuntimeError('/Generation/Parameters not found in metadata. Exiting...')

for i in systWeights:
    print '----------------'
    systName=''
    if 'pdfset' in i:
        continue

    for n,s in enumerate(i.split()):

        t=s.split("=")
        s = s.replace('.', "p")
        s = s.replace('/',"by")
        if len(t):
            if t[0]=='pdfset':
                s=t[0]+t[1]
            elif t[0] in ['muF','muR']:
                s="%s%.1f"%(t[0],float(t[1]))
                s.strip()
        else:
            print 'ERROR'

        if n:
            systName=systName+'_'+s
        else:
            systName=s

    print 'weight name:',i,', output name',systName

    if(systName==""):
        continue
    rivet = Rivet_i(systName) 
    rivet.AnalysisPath = os.environ['PWD'] 
    rivet.Analyses += [ 'Ac_tqGamma' ] 
    rivet.HistoFile = systName
    rivet.Stream = systName
    rivet.WeightName = i
    rivet.RunName = ""
    rivet.DoRootHistos = True
    rivet.CrossSection = 1.389#3.840892165
    rivet.IgnoreBeamCheck = True
    svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+systName+".root"+"' OPT='RECREATE'"]
    job += rivet
    svcMgr += THistSvc()
    
'''

rivet = Rivet_i("nominal")
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'Ac_tqGamma' ]
rivet.HistoFile = OutputYoda
#rivet.WeightName= "Var3cUpsadf"
#rivet.Stream = "Nominal"
#rivet.RunName = ""
rivet.DoRootHistos = True
rivet.CrossSection = 1.389
rivet.IgnoreBeamCheck = True
job += rivet

svcMgr += THistSvc()
svcMgr.THistSvc.Output = ["Rivet DATAFILE='"+OutputRoot+"' OPT='RECREATE'"] 
