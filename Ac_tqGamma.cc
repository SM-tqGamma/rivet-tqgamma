// -*- C++ -*-
// Fix binnings
// Axis labels for all plots
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/PartonicTops.hh"
#include "Rivet/Projections/PromptFinalState.hh"
using namespace std;
namespace Rivet {


  /// @brief Add a short analysis description here
  class Ac_tqGamma : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(Ac_tqGamma);

    /// Book histograms and initialise projections before the run
    void init() {

      Cut eta_full = Cuts::abseta < 5.0 && Cuts::pT >= 100.*MeV;
      Cut eta_lep  = Cuts::abseta < 2.5;

      // Initialise and register projections
      declare(FinalState(eta_full), "FS");

       // Parton level top quarks
      PartonicTops partonTops;
      declare(partonTops, "PartonTops");

      ///Get photons
      PromptFinalState photonfs(Cuts::abspid == PID::PHOTON && Cuts::abseta < 2.37 && Cuts::pT > 20*GeV);
      declare(photonfs, "photons");
      LeadingParticlesFinalState photonLeadfs(FinalState(-5.0, 5.0, 1.0*MeV));
      photonLeadfs.addParticleId(PID::PHOTON);
      declare(photonLeadfs, "LeadingPhoton");

      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      declare(neutrinos, "neutrinos");

      //Get electrons
      IdentifiedFinalState electronfs(Cuts::abseta < 2.47 && Cuts::pT > 1*GeV);
      electronfs.acceptIdPair(PID::ELECTRON);
      declare(electronfs, "electrons");
      DressedLeptons dressedelectrons(photonfs, electronfs, 0.1, eta_lep && (Cuts::pT >= 25*GeV));
      declare(dressedelectrons, "dressedelectrons");
      DressedLeptons fulldressedelectrons(photonfs, electronfs, 0.1, eta_full);

      //Get muons
      IdentifiedFinalState muonfs(Cuts::abseta < 2.5 && Cuts::pT > 1*GeV);
      muonfs.acceptIdPair(PID::MUON);
      declare(muonfs, "muons");
      DressedLeptons dressedmuons(photonfs, muonfs, 0.1, eta_lep && (Cuts::pT >= 25*GeV));
      declare(dressedmuons, "dressedmuons");
      DressedLeptons fulldressedmuons(photonfs, muonfs, 0.1, eta_full);


      // Jet clustering
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(fulldressedelectrons);
      vfs.addVetoOnThisFinalState(fulldressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      vfs.addVetoOnThisFinalState(photonLeadfs);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::NO_MUONS, JetAlg::DECAY_INVISIBLES);
      declare(jets, "jets");

      // Book histograms
      _fidWeights = 0.;
      _totalWeights = 0.;

      _h["ph_parent"] = bookHisto1D("ph_parent", 6, -1, 5);
      _h["ph_par_pdgId"] = bookHisto1D("ph_par_pdgId", 27, -1, 26);

      _h["jet_eta"] = bookHisto1D("jet_eta", 25, -5, 5);
      _h["jet_pT"] = bookHisto1D("jet_pT", 20, 0, 300.);
      _h["m_topph"] = bookHisto1D("m_topph", 50, 0., 1000.);
      _h["pT_topph"] = bookHisto1D("pT_topph", 35, 0., 350.);
      _h["N_jet"] = bookHisto1D("N_jet", 10, 0, 10);
      _h["photon_eta"] = bookHisto1D("photon_eta", 15, -3.0, 3.0);
      _h["photon_pT"] = bookHisto1D("photon_pT", 25, 0, 250);
      _h["top_rap"] = bookHisto1D("top_rap", 25, -5.0, 5.0);
      _h["top_pT"] = bookHisto1D("top_pT", 25, 0, 400);
      _h["top_m"] = bookHisto1D("top_m", 200, 120, 220);
      _h["lepton_pT"] = bookHisto1D("lepton_pT", 25, 0, 250);
      _h["lepton_eta"] = bookHisto1D("lepton_eta", 15, -3.0, 3.0);
      _h["HT"] = bookHisto1D("HT", 25, 0., 800.);
      _h["dPhi_topph"] = bookHisto1D("dPhi_topph", 40, -8, 8.);
      _h["dPhi_lepph"] = bookHisto1D("dPhi_lepph", 40, -8, 8.);
      _h["dR_topph"] = bookHisto1D("dR_topph", 30, 0, 6.);
      _h["dR_lepph"] = bookHisto1D("dR_lepph", 30, 0, 6.);
      _h["MET"] = bookHisto1D("MET", 10, 0., 400.);
      _h["N_bjet"] = bookHisto1D("N_bjet", 6, 0, 6);

      _h["jet_eta_prod"] = bookHisto1D("jet_eta_prod", 25, -5, 5);
      _h["jet_pT_prod"] = bookHisto1D("jet_pT_prod", 20, 0, 300.);
      _h["m_topph_prod"] = bookHisto1D("m_topph_prod", 50, 0., 1000.);
      _h["pT_topph_prod"] = bookHisto1D("pT_topph_prod", 35, 0., 350.);
      _h["N_jet_prod"] = bookHisto1D("N_jet_prod", 10, 0, 10);
      _h["photon_eta_prod"] = bookHisto1D("photon_eta_prod", 15, -3.0, 3.0);
      _h["photon_pT_prod"] = bookHisto1D("photon_pT_prod", 25, 0, 250);
      _h["top_rap_prod"] = bookHisto1D("top_rap_prod", 25, -5.0, 5.0);
      _h["top_pT_prod"] = bookHisto1D("top_pT_prod", 25, 0, 400);
      _h["top_m_prod"] = bookHisto1D("top_m_prod", 200, 120, 220);
      _h["lepton_pT_prod"] = bookHisto1D("lepton_pT_prod", 25, 0, 250);
      _h["lepton_eta_prod"] = bookHisto1D("lepton_eta_prod", 15, -3.0, 3.0);
      _h["HT_prod"] = bookHisto1D("HT_prod", 25, 0., 800.);
      _h["dPhi_topph_prod"] = bookHisto1D("dPhi_topph_prod", 40, -8, 8.);
      _h["dPhi_lepph_prod"] = bookHisto1D("dPhi_lepph_prod", 40, -8, 8.);
      _h["dR_topph_prod"] = bookHisto1D("dR_topph_prod", 30, 0, 6.);
      _h["dR_lepph_prod"] = bookHisto1D("dR_lepph_prod", 30, 0, 6.);
      _h["MET_prod"] = bookHisto1D("MET_prod", 10, 0., 400.);
      _h["N_bjet_prod"] = bookHisto1D("N_bjet_prod", 6, 0, 6);

      _h["jet_eta_decay"] = bookHisto1D("jet_eta_decay", 25, -5, 5);
      _h["jet_pT_decay"] = bookHisto1D("jet_pT_decay", 20, 0, 300.);
      _h["m_topph_decay"] = bookHisto1D("m_topph_decay", 50, 0., 1000.);
      _h["pT_topph_decay"] = bookHisto1D("pT_topph_decay", 35, 0., 350.);
      _h["N_jet_decay"] = bookHisto1D("N_jet_decay", 10, 0, 10);
      _h["photon_eta_decay"] = bookHisto1D("photon_eta_decay", 15, -3.0, 3.0);
      _h["photon_pT_decay"] = bookHisto1D("photon_pT_decay", 25, 0, 250);
      _h["top_rap_decay"] = bookHisto1D("top_rap_decay", 25, -5.0, 5.0);
      _h["top_pT_decay"] = bookHisto1D("top_pT_decay", 25, 0, 400);
      _h["top_m_decay"] = bookHisto1D("top_m_decay", 200, 120, 220);
      _h["lepton_pT_decay"] = bookHisto1D("lepton_pT_decay", 25, 0, 250);
      _h["lepton_eta_decay"] = bookHisto1D("lepton_eta_decay", 15, -3.0, 3.0);
      _h["HT_decay"] = bookHisto1D("HT_decay", 25, 0., 800.);
      _h["dPhi_topph_decay"] = bookHisto1D("dPhi_topph_decay", 40, -8, 8.);
      _h["dPhi_lepph_decay"] = bookHisto1D("dPhi_lepph_decay", 40, -8, 8.);
      _h["dR_topph_decay"] = bookHisto1D("dR_topph_decay", 30, 0, 6.);
      _h["dR_lepph_decay"] = bookHisto1D("dR_lepph_decay", 30, 0, 6.);
      _h["MET_decay"] = bookHisto1D("MET_decay", 10, 0., 400.);
      _h["N_bjet_decay"] = bookHisto1D("N_bjet_decay", 6, 0, 6);
    }


    /// Perform the per-event analysis
    void analyze(const Event& e) {

      _totalWeights += e.weight();
      FourMomentum p_miss(0.,0.,0.,0.);
      FourMomentum top(0.,0.,0.,0.);
      FourMomentum topph(0.,0.,0.,0.);
      FourMomentum lepton(0.,0.,0.,0.);

      _ph_parent_id = -1;
      _ph_par_pdgId = -1;
      _met = 0.;
      _HT = 0.;
      _m_el_ph = 0.;
      _nelectron = 0;
      _nmuon = 0;
      _nphoton = 0;
      _njet = 0;
      _nbjet = 0;

      // Get partonic tops and calculate mtt
      const Particles partontops = applyProjection<PartonicTops>(e, "PartonTops").particlesByPt();
      bool foundTop = false;
      for (const Particle& ptop : partontops) {
          const int pid = ptop.pid();
          if (pid == PID::TQUARK || pid == -PID::TQUARK) {
            top = ptop.momentum();
            foundTop = true;
          } 
      }

      // Get the selected objects, using the projections
      // Get photons
      Particles photons = applyProjection<PromptFinalState>(e, "photons").particlesByPt();
      // Get neutrinos
      Particles neutrinos = applyProjection<PromptFinalState>(e, "neutrinos").particlesByPt();
      // Get leptons
      vector<DressedLepton> electrons = applyProjection<DressedLeptons>(e, "dressedelectrons").dressedLeptons();
      Particles bare_el  = apply<IdentifiedFinalState>(e, "electrons").particles();
      vector<DressedLepton> muons = applyProjection<DressedLeptons>(e, "dressedmuons").dressedLeptons();
      Particles bare_mu  = apply<IdentifiedFinalState>(e, "muons").particles();
      // Get jets
      Jets jets = apply<JetAlg>(e, "jets").jetsByPt(Cuts::abseta < 5.0 && Cuts::pT > 25*GeV);

      // Remove photons close to leptons
      for (const Particle& el : bare_el) ifilter_discard(photons, deltaRLess(el, 0.4));
      for (const Particle& mu : bare_mu) ifilter_discard(photons, deltaRLess(mu, 0.4));
      // Remove jets close to leptons and photons
      for (const DressedLepton& el : electrons) ifilter_discard(jets, deltaRLess(el, 0.2));
      for (const DressedLepton& mu : muons) ifilter_discard(jets, deltaRLess(mu, 0.2));
      for (const Particle& ph : photons) ifilter_discard(jets, deltaRLess(ph, 0.4));
      // Remove leptons close to jets
      for (Jet j : jets) ifilter_discard(electrons, deltaRLess(j, 0.4));
      for (Jet j : jets) ifilter_discard(muons, deltaRLess(j, 0.4));

      // Missing ET 
      foreach(const Particle& p, neutrinos) p_miss+=p.momentum();
      _met = p_miss.Et()/GeV;

      // b-tagging
      Jets bjets;
      foreach(const Jet jet, jets) {
       if (jet.bTagged()) bjets += jet;
      }
      
      // Objets counters
      _nelectron = electrons.size();
      _nmuon = muons.size();
      _nphoton = photons.size();   
      _njet = jets.size();
      _nbjet = bjets.size();
      
      if (_nphoton == 0 || _njet == 0 || (_nelectron+_nmuon) != 1 ) vetoEvent;
      // dR(photon, lepton)
      if (_nelectron == 1 && _nmuon == 0) {
	lepton = electrons[0].momentum();
      }
      else if (_nelectron == 0 && _nmuon == 1) {
	lepton = muons[0].momentum();
      }

      // invariant mass of photon and electron
      if(_nelectron == 1 && _nmuon == 0) {
       _m_el_ph = (electrons[0].momentum() + photons[0].momentum()).mass()/GeV;
      }

      _HT = photons[0].pT() + lepton.pT() + _met;
      for (int k=0; k < _njet; ++k) {_HT += jets[k].pT();}

      if(foundTop) {
	topph = top + photons[0].momentum();
      }


      Particles ph_parents = photons[0].parents(Cuts::pid != 22);
      if (ph_parents.size() == 0){
	_ph_parent_id = 0;
	_ph_par_pdgId = 0;
      }
      else if(ph_parents.size() >= 1){
	_ph_par_pdgId = ph_parents[0].abspid();
	if (ph_parents[0].abspid() == 6) _ph_parent_id = 1;
	else if (ph_parents[0].abspid() == 24) _ph_parent_id = 2;
	else if (ph_parents[0].abspid() >= 11 || ph_parents[0].abspid() <= 16) _ph_parent_id = 3;
	else if (ph_parents[0].abspid() >= 1 || ph_parents[0].abspid() <= 6) _ph_parent_id = 4;
      }


      _h["ph_parent"]->fill(_ph_parent_id, e.weight());
      _h["ph_par_pdgId"]->fill(_ph_par_pdgId, e.weight());

      _h["jet_eta"]->fill(jets[0].eta(), e.weight());
      _h["jet_pT"]->fill(jets[0].pT()/GeV, e.weight());
      _h["m_topph"]->fill(topph.mass()/GeV, e.weight());
      _h["pT_topph"]->fill(topph.pT()/GeV, e.weight());
      _h["N_jet"]->fill(jets.size(), e.weight());
      _h["photon_eta"]->fill(photons[0].eta(), e.weight());
      _h["photon_pT"]->fill(photons[0].pT()/GeV, e.weight());
      _h["top_rap"]->fill(top.rap(), e.weight());
      _h["top_pT"]->fill(top.pT()/GeV, e.weight());
      _h["top_m"]->fill(top.mass()/GeV, e.weight());
      _h["lepton_pT"]->fill(lepton.pT()/GeV, e.weight());
      _h["lepton_eta"]->fill(lepton.eta(), e.weight());
      _h["HT"]->fill(_HT, e.weight());
      _h["dPhi_topph"]->fill(photons[0].phi() - top.phi(), e.weight());
      _h["dPhi_lepph"]->fill(lepton.phi() - top.phi(), e.weight());
      _h["dR_topph"]->fill(deltaR( top, photons[0].momentum() ), e.weight());
      _h["dR_lepph"]->fill(deltaR( lepton, photons[0].momentum() ), e.weight());
      _h["MET"]->fill(_met, e.weight());
      _h["N_bjet"]->fill(bjets.size(), e.weight());

      if (_ph_par_pdgId < 5 || _ph_par_pdgId == 21) {
      
	_h["jet_eta_prod"]->fill(jets[0].eta(), e.weight());
	_h["jet_pT_prod"]->fill(jets[0].pT()/GeV, e.weight());
	_h["m_topph_prod"]->fill(topph.mass()/GeV, e.weight());
	_h["pT_topph_prod"]->fill(topph.pT()/GeV, e.weight());
	_h["N_jet_prod"]->fill(jets.size(), e.weight());
	_h["photon_eta_prod"]->fill(photons[0].eta(), e.weight());
	_h["photon_pT_prod"]->fill(photons[0].pT()/GeV, e.weight());
	_h["top_rap_prod"]->fill(top.rap(), e.weight());
	_h["top_pT_prod"]->fill(top.pT()/GeV, e.weight());
	_h["top_m_prod"]->fill(top.mass()/GeV, e.weight());
	_h["lepton_pT_prod"]->fill(lepton.pT()/GeV, e.weight());
	_h["lepton_eta_prod"]->fill(lepton.eta(), e.weight());
	_h["HT_prod"]->fill(_HT, e.weight());
	_h["dPhi_topph_prod"]->fill(photons[0].phi() - top.phi(), e.weight());
	_h["dPhi_lepph_prod"]->fill(lepton.phi() - top.phi(), e.weight());
	_h["dR_topph_prod"]->fill(deltaR( top, photons[0].momentum() ), e.weight());
	_h["dR_lepph_prod"]->fill(deltaR( lepton, photons[0].momentum() ), e.weight());
	_h["MET_prod"]->fill(_met, e.weight());
	_h["N_bjet_prod"]->fill(bjets.size(), e.weight());

      }
      else {

	_h["jet_eta_decay"]->fill(jets[0].eta(), e.weight());
        _h["jet_pT_decay"]->fill(jets[0].pT()/GeV, e.weight());
        _h["m_topph_decay"]->fill(topph.mass()/GeV, e.weight());
        _h["pT_topph_decay"]->fill(topph.pT()/GeV, e.weight());
        _h["N_jet_decay"]->fill(jets.size(), e.weight());
        _h["photon_eta_decay"]->fill(photons[0].eta(), e.weight());
        _h["photon_pT_decay"]->fill(photons[0].pT()/GeV, e.weight());
        _h["top_rap_decay"]->fill(top.rap(), e.weight());
        _h["top_pT_decay"]->fill(top.pT()/GeV, e.weight());
        _h["top_m_decay"]->fill(top.mass()/GeV, e.weight());
        _h["lepton_pT_decay"]->fill(lepton.pT()/GeV, e.weight());
        _h["lepton_eta_decay"]->fill(lepton.eta(), e.weight());
        _h["HT_decay"]->fill(_HT, e.weight());
        _h["dPhi_topph_decay"]->fill(photons[0].phi() - top.phi(), e.weight());
        _h["dPhi_lepph_decay"]->fill(lepton.phi() - top.phi(), e.weight());
        _h["dR_topph_decay"]->fill(deltaR( top, photons[0].momentum() ), e.weight());
        _h["dR_lepph_decay"]->fill(deltaR( lepton, photons[0].momentum() ), e.weight());
        _h["MET_decay"]->fill(_met, e.weight());
        _h["N_bjet_decay"]->fill(bjets.size(), e.weight());
 
     }

     // Passed cuts, so get the weight
     _fidWeights += e.weight();

    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double eff = _fidWeights/_totalWeights;
      const double err = sqrt(eff*(1-eff)/numEvents());

      // Compute fiducial cross-section in fb
      const double fidCrossSection = eff * crossSection()/femtobarn;

      // Print out result
      MSG_INFO("==================================================");
      MSG_INFO("==== Total cross-section: " << 1.*crossSection()/femtobarn<< " fb");
      MSG_INFO("==== Fiducial cross-section: " << fidCrossSection << " fb");
      MSG_INFO("==== selected weighted events: " << _fidWeights << ", total weighted events: " << _totalWeights);
      MSG_INFO("==================================================");
      MSG_INFO("==== Selection efficiency: " << eff << " +/- " << err << " (statistical error)");
      MSG_INFO("==================================================");

      const double normto(crossSection() / femtobarn / sumOfWeights());
      for (auto &hist : _h) {  
	scale(hist.second, normto);
	//normalize(hist.second);
      }

    } /// finalize() end

    /// @name Histograms
    private:
    map<string, Histo1DPtr> _h;
    double _fidWeights;
    double _totalWeights;
    int  _ph_parent_id = -1;
    int _ph_par_pdgId = -1;
    double _met;
    double _HT;
    double _m_el_ph;
    double _nelectron;
    double _nmuon;
    double _nphoton;
    double _njet;
    double _nbjet;

  }; /// Analysis class end

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(Ac_tqGamma);

}
